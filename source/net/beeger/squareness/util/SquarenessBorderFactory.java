/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JFrame;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicBorders;

import net.beeger.squareness.SquarenessConstants;
import net.beeger.squareness.SquarenessLookAndFeel;

/**
 * The SquarenessBorderFactory is a static factory that creates all the borders that are needed by the Look And Feel.
 */
public class SquarenessBorderFactory
{
  /**
   * Return the border for the controls.<br>
   * The returned border also contains an outer border with the color of the parent of the component for which
   * it is used, which creates a little space between neighboring components.
   * 
   * @return The border for the controls.
   */
  public static Border getControlBorderWithMargin ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ParentBackgroundBorder(),
                                                         new BorderUIResource.CompoundBorderUIResource(
                                                            new ControlBorder(),
                                                            new BorderUIResource.CompoundBorderUIResource(
                                                               new BorderUIResource.EmptyBorderUIResource(1, 1, 1, 1),
                                                               new BasicBorders.MarginBorder())));
  }

  /**
   * Return the border for the controls.<br>
   * The same as {@link #getControlBorderWithMargin()} but without the parent color outer border.
   *
   * @return The border for the controls.
   */
  public static Border getNonSpacingControlBorderWithMargin ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ControlBorder(),
                                                         new BorderUIResource.CompoundBorderUIResource(
                                                            new BorderUIResource.EmptyBorderUIResource(1, 1, 1, 1),
                                                            new BasicBorders.MarginBorder()));
  }

  /**
   * Return the border for the controls.<br>
   * The same as {@link #getControlBorderWithMargin()} but without the inner margin border.
   *
   * @return The border for the controls.
   */
  public static Border getControlBorderWithoutMargin ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ParentBackgroundBorder(),
                                                         new BorderUIResource.CompoundBorderUIResource(
                                                            new ControlBorder(),
                                                            new BorderUIResource.EmptyBorderUIResource(1, 1, 1, 1)));
  }

  public static Border getProgressBarBorder ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ParentBackgroundBorder(),new ControlBorder());
  }

  /**
   * Return the border for the controls.<br>
   * The same as {@link #getControlBorderWithMargin()} but without the inner margin border.
   *
   * @return The border for the controls.
   */
  public static Border getNonSpacingControlBorderWithoutMargin ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ControlBorder(),
                                                         new BorderUIResource.EmptyBorderUIResource(1, 1, 1, 1));
  }

  /**
   * Return the rollover border for buttons<br>
   * Used for rollover buttons on tool bars.
   *
   * @return The created border.
   */
  public static Border getButtonRolloverBorder ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ParentBackgroundBorder(),
                                                         new BorderUIResource.CompoundBorderUIResource(
                                                            new ButtonRolloverBorder(),
                                                            new RolloverMarginBorder()));
  }

  /**
   * Return the non rollover border for buttons<br>
   * Used for non rollover buttons on tool bars.
   *
   * @return The created border.
   */
  public static Border getButtonNonRolloverBorder ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new ParentBackgroundBorder(),
                                                         new BorderUIResource.CompoundBorderUIResource(
                                                            new ControlBorder(),
                                                            new RolloverMarginBorder()));
  }

  /**
   * Return the border for table headers.
   *
   * @return The created border.
   */
  public static Border getTableHeaderCellBorder ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new TableHeaderCellBorder(),
                                                         new BorderUIResource.EmptyBorderUIResource(1, 2, 1, 2));
  }

  /**
   * Return the border for menu items.
   *
   * @return The created border.
   */
  public static Border getMenuItemBorder ()
  {
    return new MenuItemBorder();
  }

  /**
   * Return the border for the controls.<br>
   * The same as {@link #getControlBorderWithMargin()} but without the inner margin border.
   *
   * @return The border for the controls.
   */
  public static Border getInternalFrameBorder ()
  {
    return new BorderUIResource.CompoundBorderUIResource(new InternalFrameBorder(),
                                                         new BorderUIResource.LineBorderUIResource(SquarenessLookAndFeel.getCurrentSquarenessTheme().getWindowBackgroundColor(),1));
  }

  /**
   * The control border.<br>
   * The color of the border changes to reflect the status of the control (disabled, default button, normal)
   */
  private static class ControlBorder extends AbstractBorder implements UIResource
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      Color oldColor = graphics.getColor();

      if (!component.isEnabled())
      {
        graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getDisabledBorderColor());
      }
      else if (component instanceof JButton && ((JButton) component).isDefaultButton())
      {
        graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getDefaultButtonBorderColor());
      }
      else
      {
        graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
      }
      graphics.drawLine(x, y, x + width - 1, y);
      graphics.drawLine(x, y, x, y + height - 1);
      graphics.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
      graphics.drawLine(x + width - 1, y, x + width - 1, y + height - 1);

      graphics.setColor(oldColor);
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component)
    {
      return getBorderInsets(component, new Insets(0, 0, 0, 0));
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @param insets The object to put the newly calculated insets into.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component, Insets insets)
    {
      insets.top = insets.left = insets.bottom = insets.right = 1;
      return insets;
    }

    /**
     * Is this border opaque?
     *
     * @return true, if this border is opaque, false otherwise
     */
    public boolean isBorderOpaque ()
    {
      return true;
    }
  }

  /**
   * This border has the color of the parent of the component for which it is painted.
   */
  private static class ParentBackgroundBorder extends AbstractBorder implements UIResource
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      Color oldColor = graphics.getColor();

      graphics.setColor(component.getParent() != null
                        ? component.getParent().getBackground()
                        : SquarenessLookAndFeel.getCurrentSquarenessTheme().getWindowBackgroundColor());
      graphics.drawLine(x, y, x + width - 1, y);
      graphics.drawLine(x, y, x, y + height - 1);
      graphics.drawLine(x, y + height - 1, x + width - 1, y + height - 1);
      graphics.drawLine(x + width - 1, y, x + width - 1, y + height - 1);

      graphics.setColor(oldColor);
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component)
    {
      return getBorderInsets(component, new Insets(0, 0, 0, 0));
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @param insets The object to put the newly calculated insets into.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component, Insets insets)
    {
      insets.top = insets.left = insets.bottom = insets.right = 1;
      return insets;
    }

    /**
     * Is this border opaque?
     *
     * @return true, if this border is opaque, false otherwise
     */
    public boolean isBorderOpaque ()
    {
      return true;
    }
  }

  private static class InternalFrameBorder extends AbstractBorder implements UIResource
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      Color oldColor = graphics.getColor();


      if (component instanceof JInternalFrame)
      {
        JInternalFrame internalFrame = (JInternalFrame) component;
        if (internalFrame.isSelected())
        {
          graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
        }
        else
        {
          graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getInactiveWindowBorderColor());
        }
      }
      else if (component instanceof JFrame)
      {
        JFrame frame = (JFrame) component;
        if (frame.isActive())
        {
          graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
        }
        else
        {
          graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getInactiveWindowBorderColor());
        }
      }

      graphics.translate(x, y);

      graphics.drawLine(0, 0, width - 1, 0);
      graphics.drawLine(0, 0, 0, height - 1);
      graphics.drawLine(0, height - 1, width - 1, height - 1);
      graphics.drawLine(width - 1, y, width - 1, height - 1);

      if (getBorderInsets(component).top == 2)
      {
        graphics.drawLine(1, 1, width - 2, 1);
        graphics.drawLine(1, 1, 1, height - 2);
        graphics.drawLine(1, height - 2, width - 2, height - 2);
        graphics.drawLine(width - 2, y, width - 2, height - 2);
      }

      graphics.translate(-x,-y);
      graphics.setColor(oldColor);
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component)
    {
      return getBorderInsets(component, new Insets(0, 0, 0, 0));
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @param insets The object to put the newly calculated insets into.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component, Insets insets)
    {
      if (component instanceof JInternalFrame
          && (((JInternalFrame)component).getClientProperty("JInternalFrame.isPalette") != null))
      {
        insets.top =  1;
      }
      else
      {
        insets.top = 2;
      }
      insets.bottom = insets.top;
      insets.right = insets.top;
      insets.left = insets.top;
      return insets;
    }

    /**
     * Is this border opaque?
     *
     * @return true, if this border is opaque, false otherwise
     */
    public boolean isBorderOpaque ()
    {
      return true;
    }  }

  /**
   * The border for table headers.
   */
  private static class TableHeaderCellBorder extends AbstractBorder implements UIResource
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      Color oldColor = graphics.getColor();

      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalBorderColor());
      graphics.drawLine(x, y + height - 1, x + width - 1, y + height - 1);

      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalControlBackgroundColor().darker());
      graphics.drawLine(x + width - 2, y + 3, x + width - 2, y + height - 4);

      graphics.setColor(SquarenessLookAndFeel.getCurrentSquarenessTheme().getNormalControlBackgroundColor().brighter());
      graphics.drawLine(x + width - 1, y + 3, x + width - 1, y + height - 4);

      graphics.setColor(oldColor);
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component)
    {
      return getBorderInsets(component, new Insets(0, 0, 0, 0));
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @param insets The object to put the newly calculated insets into.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component, Insets insets)
    {
      insets.top = insets.left = insets.bottom = insets.right = 1;
      return insets;
    }

    /**
     * Is this border opaque?
     *
     * @return true, if this border is opaque, false otherwise
     */
    public boolean isBorderOpaque ()
    {
      return true;
    }
  }

  /**
   * The rollover border for buttons.
   */
  private static class ButtonRolloverBorder extends ControlBorder
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      AbstractButton button = (AbstractButton) component;
      ButtonModel model = button.getModel();
      boolean isRollover = ((Boolean) button.getClientProperty(SquarenessConstants.ROLLOVER_CLENT_PROPERTY_KEY)).booleanValue();
      if (button.isEnabled()
          && (isRollover
              || model.isArmed() && model.isPressed()
              || button.hasFocus()
              || button.isSelected()))
      {
        super.paintBorder(component, graphics, x, y, width, height);
      }
    }
  }

  /**
   * Margin Border for rollover borders.
   */
  private static class RolloverMarginBorder extends EmptyBorder
  {
    /**
     * Create object.
     */
    public RolloverMarginBorder ()
    {
      super(3, 3, 3, 3);
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component)
    {
      return getBorderInsets(component, new Insets(0, 0, 0, 0));
    }

    /**
     * Return the insets of this border.
     *
     * @param component The component for which to return the insets of this border.
     * @param insets The object to put the newly calculated insets into.
     * @return The calculated insets.
     */
    public Insets getBorderInsets (Component component, Insets insets)
    {
      Insets margin = null;

      if (component instanceof AbstractButton)
      {
        margin = ((AbstractButton) component).getMargin();
      }
      if (margin == null || margin instanceof UIResource)
      {
        insets.left = left;
        insets.top = top;
        insets.right = right;
        insets.bottom = bottom;
      }
      else
      {
        insets.left = margin.left;
        insets.top = margin.top;
        insets.right = margin.right;
        insets.bottom = margin.bottom;
      }
      return insets;
    }
  }

  /**
   * Menu item border.
   */
  private static class MenuItemBorder extends ControlBorder
  {
    /**
     * Paint the border for the given component.
     *
     * @param component The component for which to paint the border.
     * @param graphics The graphics object to use for painting.
     * @param x The x position of the component.
     * @param y The y position of the component.
     * @param width The width of the component.
     * @param height The height of the component.
     */
    public void paintBorder (Component component, Graphics graphics, int x, int y, int width, int height)
    {
      JMenuItem menuItem = (JMenuItem) component;
      ButtonModel model = menuItem.getModel();
      if (model.isArmed() || (menuItem instanceof JMenu && menuItem.isSelected()))
      {
        super.paintBorder(component, graphics, x, y, width, height);
      }
    }
  }

}