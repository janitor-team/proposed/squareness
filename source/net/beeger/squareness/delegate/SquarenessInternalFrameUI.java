/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;

import net.beeger.squareness.util.SquarenessTitlePane;

/**
 * The Squareness Internal Frame UI delegate.
 */
public class SquarenessInternalFrameUI extends BasicInternalFrameUI
{
  /**
   * Create the UI delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return new SquarenessInternalFrameUI((JInternalFrame) component);
  }

  

  /**
   * Create the UI delegate.
   *
   * @param internalFrame The internal frame this delegate will be used for.
   */
  public SquarenessInternalFrameUI (JInternalFrame internalFrame)
  {
    super(internalFrame);
  }

  /**
   * Create the title bar of the internal frame.
   *
   * @param internalFrame The internalframe for which to create the title bar.
   * @return The created title bar.
   */
  protected JComponent createNorthPane (JInternalFrame internalFrame)
  {
    _titlePane = new SquarenessInternalFrameTitlePane(internalFrame);
    return _titlePane;
  }

  /**
   * The class for title bars of internal frames.
   */
  protected static class SquarenessInternalFrameTitlePane extends SquarenessTitlePane
  {
    public SquarenessInternalFrameTitlePane (JInternalFrame frame)
    {
      _frame = frame;
      _frame.addPropertyChangeListener(new PropertyChangeListener()
      {
        public void propertyChange (PropertyChangeEvent evt)
        {
          update();
          _frame.revalidate();
          _frame.repaint();
        }
      });
    }

    public boolean isLeftToRight ()
    {
      return _frame.getComponentOrientation().isLeftToRight();
    }

    protected boolean isIconifiable ()
    {
      return _frame.isIconifiable();
    }

    protected boolean isMaximizable ()
    {
      return _frame.isMaximizable();
    }

    protected boolean isClosable ()
    {
      return _frame.isClosable();
    }

    protected void close ()
    {
      if (isClosable())
      {
        _frame.doDefaultCloseAction();
      }
    }

    protected void iconify ()
    {
      if (isIconifiable())
      {
        if (!isIconified())
        {
          try
          {
            _frame.setIcon(true);
          }
          catch (PropertyVetoException e1)
          {
          }
        }
        else
        {
          try
          {
            _frame.setIcon(false);
          }
          catch (PropertyVetoException e1)
          {
          }
        }
      }
    }

    protected void maximize ()
    {
      if (isMaximizable())
      {
        if (!isMaximized())
        {
          try
          {
            _frame.setMaximum(true);
          }
          catch (PropertyVetoException e5)
          {
          }
        }
        else
        {
          try
          {
            _frame.setMaximum(false);
          }
          catch (PropertyVetoException e6)
          {
          }
        }
      }
    }

    protected void restore ()
    {
      if (isMaximizable() && isMaximized())
      {
        try
        {
          _frame.setMaximum(false);
        }
        catch (PropertyVetoException e4)
        {
        }
      }
      else if (isIconifiable() && isIconified())
      {
        try
        {
          _frame.setIcon(false);
        }
        catch (PropertyVetoException e4)
        {
        }
      }
    }

    protected boolean isMaximized ()
    {
      return _frame.isMaximum();
    }

    protected boolean isIconified ()
    {
      return _frame.isIcon();
    }

    protected String getTitle ()
    {
      return _frame.getTitle();
    }

    private JInternalFrame _frame;
  }

  private SquarenessInternalFrameTitlePane _titlePane;
}