/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.plaf.basic.BasicToggleButtonUI;
import javax.swing.text.View;

import net.beeger.squareness.SquarenessConstants;
import net.beeger.squareness.util.SquarenessButtonPainter;
import net.beeger.squareness.util.SquarenessListenerFactory;

/**
 * The Squareness Toggle Button UI delegate.
 */
public class SquarenessToggleButtonUI extends BasicToggleButtonUI
{
  /**
   * Create the UI delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return _buttonUI;
  }

  /**
   * Install the UI delegate for the given component
   * 
   * @param component The component for which to install the ui delegate.
   */
  public void installUI (JComponent component)
  {
    component.putClientProperty(SquarenessConstants.ROLLOVER_CLENT_PROPERTY_KEY, Boolean.FALSE);
    component.addMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.installUI(component);
  }

  /**
   * Uninstall the UI delegate from the given component.
   *
   * @param component The component from which to uninstall the UI delegate.
   */
  public void uninstallUI (JComponent component)
  {
    component.removeMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.uninstallUI(component);
  }

  /**
   * Paint the component.
   * 
   * @param graphics  The graphics resource used to paint the component
   * @param component The component to paint.
   */
  public void paint (Graphics graphics, JComponent component)
  {
    Color oldColor = graphics.getColor();

    SquarenessButtonPainter.paintButton(graphics, (AbstractButton) component);

    AbstractButton b = (AbstractButton) component;
    ButtonModel model = b.getModel();

    Dimension size = b.getSize();
    FontMetrics fm = graphics.getFontMetrics();

    Insets i = component.getInsets();

    Rectangle viewRect = new Rectangle(size);

    viewRect.x += i.left;
    viewRect.y += i.top;
    viewRect.width -= (i.right + viewRect.x);
    viewRect.height -= (i.bottom + viewRect.y);

    Rectangle iconRect = new Rectangle();
    Rectangle textRect = new Rectangle();

    Font f = component.getFont();
    graphics.setFont(f);

    // layout the text and icon
    String text = SwingUtilities.layoutCompoundLabel(component, fm, b.getText(), b.getIcon(),
                                                     b.getVerticalAlignment(), b.getHorizontalAlignment(),
                                                     b.getVerticalTextPosition(), b.getHorizontalTextPosition(),
                                                     viewRect, iconRect, textRect,
                                                     b.getText() == null ? 0 : b.getIconTextGap());

    graphics.setColor(b.getBackground());

    if (model.isArmed() && model.isPressed() || model.isSelected())
    {
      paintButtonPressed(graphics, b);
    }

    // Paint the Icon
    if (b.getIcon() != null)
    {
      paintIcon(graphics, b, iconRect);
    }
	
    // Draw the Text
    if (text != null && !text.equals(""))
    {
      View v = (View) component.getClientProperty(BasicHTML.propertyKey);
      if (v != null)
      {
        v.paint(graphics, textRect);
      }
      else
      {
        paintText(graphics, b, textRect, text);
      }
    }
	
    // draw the dashed focus line.
    if (b.isFocusPainted() && b.hasFocus())
    {
      paintFocus(graphics, b, viewRect, textRect, iconRect);
    }

    graphics.setColor(oldColor);
  }

  private static SquarenessToggleButtonUI _buttonUI = new SquarenessToggleButtonUI();
}