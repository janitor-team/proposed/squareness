/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicCheckBoxUI;

import net.beeger.squareness.SquarenessConstants;
import net.beeger.squareness.util.SquarenessListenerFactory;

/**
 * The Squareness Check Box UI delegate.
 */
public class SquarenessCheckBoxUI extends BasicCheckBoxUI
{
  /**
   * Create the UI delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return _buttonUI;
  }

  /**
   * Install the UI delegate for the given component
   * 
   * @param component The component for which to install the ui delegate.
   */
  public void installUI (JComponent component)
  {
    component.putClientProperty(SquarenessConstants.ROLLOVER_CLENT_PROPERTY_KEY, Boolean.FALSE);
    component.addMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.installUI(component);
  }

  /**
   * Uninstall the UI delegate.
   *
   * @param component The component from which to uninstall the UI delegate.
   */
  public void uninstallUI (JComponent component)
  {
    component.removeMouseListener(SquarenessListenerFactory.getButtonRolloverMouseListener());
    super.uninstallUI(component);
  }

  private static SquarenessCheckBoxUI _buttonUI = new SquarenessCheckBoxUI();
}