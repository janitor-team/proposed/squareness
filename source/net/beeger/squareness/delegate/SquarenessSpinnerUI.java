/*
  Copyright (c) 2003-2006, Robert F. Beeger (robert at beeger dot net)
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice, this list
        of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright notice, this
        list of conditions and the following disclaimer in the documentation and/or other
        materials provided with the distribution.
      * Neither the name of 'Robert F. Beeger' nor the names of his contributors may be
        used to endorse or promote products derived from this software without specific
        prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
  OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
  TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Visit http://squareness.beeger.net/ for new releases of Squareness Look And Feel and other
  skins from the Squareness series.
*/
package net.beeger.squareness.delegate;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSpinnerUI;

import net.beeger.squareness.SquarenessConstants;

/**
 * The Squareness Spinner UI Delegate.
 */
public class SquarenessSpinnerUI extends BasicSpinnerUI
{
  /**
   * Create the UI delegate for the given component
   * 
   * @param component The component for which to create the ui delegate
   * @return The created ui delegate
   */
  public static ComponentUI createUI (JComponent component)
  {
    return new SquarenessSpinnerUI();
  }

  /**
   * Install the UI delegate for the given component.
   *
   * @param component The component for which to install the UI delegate.
   */
  public void installUI (JComponent component)
  {
    super.installUI(component);
    _nextButton.addActionListener(SwingUtilities.getUIActionMap(spinner).get("increment"));
    _nextButton.addMouseListener((MouseListener) SwingUtilities.getUIActionMap(spinner).get("increment"));
    _previousButton.addActionListener(SwingUtilities.getUIActionMap(spinner).get("decrement"));
    _previousButton.addMouseListener((MouseListener) SwingUtilities.getUIActionMap(spinner).get("decrement"));
  }

  /**
   * Create the button which increases the value of the spinner.
   *
   * @return The created button
   */
  protected Component createNextButton ()
  {
    _nextButton = new JButton(UIManager.getIcon(SquarenessConstants.SPIN_UP_KEY));
    _nextButton.setPreferredSize(new Dimension(16, 16));
    _nextButton.setMinimumSize(new Dimension(5, 5));
    _nextButton.setHorizontalAlignment(SwingConstants.CENTER);
    _nextButton.setVerticalAlignment(SwingConstants.CENTER);
    return _nextButton;

  }

  /**
   * Create the button which decreases the value of the spinner.
   *
   * @return The create button
   */
  protected Component createPreviousButton ()
  {
    _previousButton = new JButton(UIManager.getIcon(SquarenessConstants.SPIN_DOWN_KEY));
    _previousButton.setPreferredSize(new Dimension(16, 16));
    _previousButton.setMinimumSize(new Dimension(5, 5));
    _previousButton.setHorizontalAlignment(SwingConstants.CENTER);
    _previousButton.setVerticalAlignment(SwingConstants.CENTER);
    return _previousButton;
  }

  private JButton _previousButton;
  private JButton _nextButton;

}